package com.ksa.coffeehellapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoffeeHellApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoffeeHellApiApplication.class, args);
	}

}
