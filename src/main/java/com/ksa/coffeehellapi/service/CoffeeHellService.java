package com.ksa.coffeehellapi.service;

import com.ksa.coffeehellapi.entity.CoffeeHell;
import com.ksa.coffeehellapi.entity.Member;
import com.ksa.coffeehellapi.model.CoffeeHell.CoffeeHellItem;
import com.ksa.coffeehellapi.model.CoffeeHell.CoffeeHellRequest;
import com.ksa.coffeehellapi.model.CoffeeHell.CoffeeHellResponse;
import com.ksa.coffeehellapi.model.member.MemberItem;
import com.ksa.coffeehellapi.repository.CoffeeHellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CoffeeHellService {
    private final CoffeeHellRepository coffeeHellRepository;

    public void setCoffeeHell(Member member, CoffeeHellRequest request) {
        CoffeeHell addData = new CoffeeHell();

        addData.setMember(member);
        addData.setCoffeePrice(request.getCoffeePrice());
        addData.setDateCoffee(request.getDateCoffee());

        coffeeHellRepository.save(addData);
    }

    public List<CoffeeHellItem> getCoffeeHells() {
        List<CoffeeHell> originList = coffeeHellRepository.findAll();
        List<CoffeeHellItem> result = new LinkedList<>();

        for (CoffeeHell coffeeHell : originList) {
            CoffeeHellItem addItem = new CoffeeHellItem();
            addItem.setMemberId(coffeeHell.getMember().getId());
            addItem.setCoffeePrice(coffeeHell.getCoffeePrice());

            result.add(addItem);
        }
        return result;
    }

    public CoffeeHellResponse getCoffeeHell(long id) {
        CoffeeHell originData = coffeeHellRepository.findById(id).orElseThrow();

        CoffeeHellResponse response = new CoffeeHellResponse();
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setMemberPhoneNumber(originData.getMember().getPhoneNumber());
        response.setCoffeePrice(originData.getCoffeePrice());
        response.setDateCoffee(originData.getDateCoffee());

        return response;
    }
}
