package com.ksa.coffeehellapi.service;

import com.ksa.coffeehellapi.entity.Member;
import com.ksa.coffeehellapi.model.member.MemberBaseInfoChangeRequest;
import com.ksa.coffeehellapi.model.member.MemberItem;
import com.ksa.coffeehellapi.model.member.MemberRequest;
import com.ksa.coffeehellapi.model.member.MemberResponse;
import com.ksa.coffeehellapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {return memberRepository.findById(id).orElseThrow();}

    public void setMember(MemberRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originData = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originData) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();

        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;
    }

    public void putMemberRequest(long id, MemberBaseInfoChangeRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setName(request.getName());
        originData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(originData);
    }
}
