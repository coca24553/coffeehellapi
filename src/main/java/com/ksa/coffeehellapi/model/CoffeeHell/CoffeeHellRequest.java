package com.ksa.coffeehellapi.model.CoffeeHell;

import com.ksa.coffeehellapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CoffeeHellRequest {
    private Double coffeePrice;
    private LocalDate dateCoffee;
}
