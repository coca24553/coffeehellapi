package com.ksa.coffeehellapi.model.CoffeeHell;

import com.ksa.coffeehellapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CoffeeHellResponse {
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private Double coffeePrice;
    private LocalDate dateCoffee;
}
