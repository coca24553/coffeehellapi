package com.ksa.coffeehellapi.model.CoffeeHell;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoffeeHellItem {
    private Long memberId;
    private Double coffeePrice;
}
