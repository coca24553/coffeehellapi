package com.ksa.coffeehellapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRequest {
    private String name;
    private String phoneNumber;
}
