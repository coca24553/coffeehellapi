package com.ksa.coffeehellapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberBaseInfoChangeRequest {
    private String name;
    private String phoneNumber;
}
