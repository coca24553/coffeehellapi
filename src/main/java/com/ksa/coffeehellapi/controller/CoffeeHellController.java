package com.ksa.coffeehellapi.controller;

import com.ksa.coffeehellapi.entity.Member;
import com.ksa.coffeehellapi.model.CoffeeHell.CoffeeHellItem;
import com.ksa.coffeehellapi.model.CoffeeHell.CoffeeHellRequest;
import com.ksa.coffeehellapi.model.CoffeeHell.CoffeeHellResponse;
import com.ksa.coffeehellapi.service.CoffeeHellService;
import com.ksa.coffeehellapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/coffee-hell")
public class CoffeeHellController {
    private final MemberService memberService;
    private final CoffeeHellService coffeeHellService;

    @PostMapping("/new/member-id/{memberId}")
    public String setCoffeeHell(@PathVariable long memberId, @RequestBody CoffeeHellRequest request) {
        Member member = memberService.getData(memberId);
        coffeeHellService.setCoffeeHell(member, request);

        return "ok";
    }

    @GetMapping("/all")
    public List<CoffeeHellItem> getCoffeeHells() {
        return coffeeHellService.getCoffeeHells();
    }

    @GetMapping("/detail/{id}")
    public CoffeeHellResponse getCoffeeHell(@PathVariable long id) {
        return coffeeHellService.getCoffeeHell(id);
    }
}
