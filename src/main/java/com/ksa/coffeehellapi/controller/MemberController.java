package com.ksa.coffeehellapi.controller;

import com.ksa.coffeehellapi.model.CoffeeHell.CoffeeHellResponse;
import com.ksa.coffeehellapi.model.member.MemberBaseInfoChangeRequest;
import com.ksa.coffeehellapi.model.member.MemberItem;
import com.ksa.coffeehellapi.model.member.MemberRequest;
import com.ksa.coffeehellapi.model.member.MemberResponse;
import com.ksa.coffeehellapi.service.MemberService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public String setMember(@RequestBody MemberRequest request) {
        memberService.setMember(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

    @PutMapping("/base-info/{id}")
    public String putBaseInfo(@PathVariable long id, @RequestBody MemberBaseInfoChangeRequest request) {
        memberService.putMemberRequest(id, request);

        return "ok";
    }
}
