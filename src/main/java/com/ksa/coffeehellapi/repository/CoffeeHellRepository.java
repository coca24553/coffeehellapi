package com.ksa.coffeehellapi.repository;

import com.ksa.coffeehellapi.entity.CoffeeHell;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoffeeHellRepository extends JpaRepository<CoffeeHell, Long> {
}
