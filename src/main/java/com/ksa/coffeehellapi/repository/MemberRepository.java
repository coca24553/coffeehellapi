package com.ksa.coffeehellapi.repository;

import com.ksa.coffeehellapi.entity.Member;
import org.hibernate.type.descriptor.converter.spi.JpaAttributeConverter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
